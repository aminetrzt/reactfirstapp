import React from 'react' 

export default function Main() {
	return (
		<div>
			<section class="bg-home bg-light d-table w-100" style= {{ backgroundImage : "url('images/home/01.png')" }} id="home">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12">
                        <div class="title-heading mt-5">
                            <h6 class="sub-title">Looking for a Designer !</h6>
                            <h1 class="heading text-primary mb-3">I'm Cristino Murphy</h1>
                            <p class="para-desc text-muted">Obviously I'm a Web Designer. Web Developer with over 3 years of experience. Experienced with all stages of the development cycle for dynamic web projects.</p>
                            <div class="mt-4 pt-2">
                                <a href="javascript:void(0)" class="btn btn-primary rounded mb-2 mr-2">Hire me</a>
                                <a href="javascript:void(0)" class="btn btn-outline-primary rounded mb-2">Download CV <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-download fea icon-sm"><path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path><polyline points="7 10 12 15 17 10"></polyline><line x1="12" y1="15" x2="12" y2="3"></line></svg></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="index.html#about" data-scroll-nav="1" class="mouse-icon rounded-pill bg-transparent mouse-down">
                <span class="wheel position-relative d-block mover"></span>
            </a>
        </section>
		</div>
	)
};