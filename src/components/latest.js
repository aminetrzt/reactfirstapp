import React from 'react'


export default function () {
	return (
		<>
			<section class="section bg-light pb-3" id="news">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <div class="titles">
                                <h4 class="title title-line text-uppercase mb-4 pb-4">Latest News &amp; Blog</h4>
                                <span></span>
                            </div>
                            <p class="text-muted mx-auto para-desc mb-0">Obviously I'm a Web Designer. Experienced with all stages of the development cycle for dynamic web projects.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                        <div class="blog-post rounded shadow">
                            <img src="images/blog/01.jpg" class="img-fluid rounded-top" alt=""/>
                            <div class="content pt-4 pb-4 p-3">
                                <ul class="list-unstyled d-flex justify-content-between post-meta">
                                            <li><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user fea icon-sm mr-1"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg><a href="javascript:void(0)" class="text-dark">Cristino</a></li> 
                                            <li><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tag fea icon-sm mr-1"><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7.01" y2="7"></line></svg><a href="javascript:void(0)" class="text-dark">Branding</a></li>                                    
                                        </ul>  
                                <h5 class="mb-3"><a href="page-blog-detail.html" class="title text-dark">Our Home Entertainment has Evolved Significantly</a></h5> 
                                <ul class="list-unstyled mb-0 pt-3 border-top d-flex justify-content-between">
                                    <li><a href="javascript:void(0)" class="text-dark">Read More <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right fea icon-sm"><polyline points="9 18 15 12 9 6"></polyline></svg></a></li>
                                    <li><i class="mdi mdi-calendar-edit mr-1"></i>10th April, 2020</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                        <div class="blog-post rounded shadow">
                            <img src="images/blog/02.jpg" class="img-fluid rounded-top" alt=""/>
                            <div class="content pt-4 pb-4 p-3">
                                <ul class="list-unstyled d-flex justify-content-between post-meta">
                                            <li><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user fea icon-sm mr-1"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg><a href="javascript:void(0)" class="text-dark">Cristino</a></li> 
                                            <li><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tag fea icon-sm mr-1"><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7.01" y2="7"></line></svg><a href="javascript:void(0)" class="text-dark">Branding</a></li>                                    
                                        </ul> 
                                <h5 class="mb-3"><a href="page-blog-detail.html" class="title text-dark">These Are The Voyages of The Starship Enterprise</a></h5>
                                <ul class="list-unstyled mb-0 pt-3 border-top d-flex justify-content-between">
                                    <li><a href="javascript:void(0)" class="text-dark">Read More <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right fea icon-sm"><polyline points="9 18 15 12 9 6"></polyline></svg></a></li>
                                    <li><i class="mdi mdi-calendar-edit mr-1"></i>10th April, 2020</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                        <div class="blog-post rounded shadow">
                            <img src="images/blog/03.jpg" class="img-fluid rounded-top" alt=""/>
                            <div class="content pt-4 pb-4 p-3">
                                <ul class="list-unstyled d-flex justify-content-between post-meta">
                                            <li><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user fea icon-sm mr-1"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg><a href="javascript:void(0)" class="text-dark">Cristino</a></li> 
                                            <li><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tag fea icon-sm mr-1"><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7.01" y2="7"></line></svg><a href="javascript:void(0)" class="text-dark">Branding</a></li>                                    
                                        </ul> 
                                <h5 class="mb-3"><a href="page-blog-detail.html" class="title text-dark">Three Reasons Visibility Matters in Supply Chain</a></h5>
                                <ul class="list-unstyled mb-0 pt-3 border-top d-flex justify-content-between">
                                    <li><a href="javascript:void(0)" class="text-dark">Read More <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right fea icon-sm"><polyline points="9 18 15 12 9 6"></polyline></svg></a></li>
                                    <li><i class="mdi mdi-calendar-edit mr-1"></i>10th April, 2020</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid mt-100 mt-60">
                <div class="rounded-pill py-5" style={{ background: "url('images/hireme.jpg')" }}>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-12 text-center">
                                <h4 class="text-light">I Am Available For Freelancer Projects.</h4>
                                <p class="text-white-50 mx-auto mt-4 para-desc">Obviously I'm a Web Designer. Experienced with all stages of the development cycle for dynamic web projects.</p>
                                <div class="mt-4">
                                    <a href="index.html#contact" class="btn btn-primary mouse-down">Hire me <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down fea icon-sm"><polyline points="6 9 12 15 18 9"></polyline></svg></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		</>
	)
}