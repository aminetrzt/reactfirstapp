import React from 'react'

import Header from '../header'
import Footer from '../footer'

function Content () {
	return <h1>Contact page</h1>
}

export default function () {
	return (
		<>
		<Header/>
		<br/><br/><br/><br/><br/>
		<Content/>
		<Footer/>
		</>
	)
}