import React from 'react'



export default function () {
	return(
		<>
			<section class="section pt-5 mt-3">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="custom-form mb-sm-30">
                            <div id="message"></div>
                            <form method="post" action="http://shreethemes.in/cristino/layouts/php/contact.php" name="contact-form" id="contact-form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-6">
                                                <div class="form-group">
                                                    <input name="name" id="name" type="text" class="form-control border rounded" placeholder="First Name :"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-6">
                                                <div class="form-group">
                                                    <input name="email" id="email" type="email" class="form-control border rounded" placeholder="Your email :"/>
                                                </div> 
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <input name="subject" id="subject" class="form-control border rounded" placeholder="Your subject :"/>
                                                </div>                                                                               
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <textarea name="comments" id="comments" rows="4" class="form-control border rounded" placeholder="Your Message :"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-right">
                                        <input type="submit" id="submit" name="send" class="submitBnt btn btn-primary" value="Send Message"/>
                                        <div id="simple-msg"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		</>
	)
}