import React from 'react'


export default function () {
	return (
		<>
			<footer class="footer bg-dark">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <a href="index.html#"><img src="images/logo-light.png" alt="" height="20"/></a>
                        <p class="para-desc mx-auto mt-5">Obviously I'm a Web Designer. Experienced with all stages of the development cycle for dynamic web projects.</p>
                        <ul class="list-unstyled mb-0 mt-4 social-icon">
                            <li class="list-inline-item"><a href="javascript:void(0)"><i class="mdi mdi-facebook"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)"><i class="mdi mdi-twitter"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)"><i class="mdi mdi-instagram"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)"><i class="mdi mdi-vimeo"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)"><i class="mdi mdi-google-plus"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)"><i class="mdi mdi-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <footer class="footer footer-bar bg-dark">
            <div class="container text-foot text-center">
                <p class="mb-0">© 2019-20 Cristino. Design with <i class="mdi mdi-heart text-danger"></i> by <a href="http://www.shreethemes.in" target="_blank" class="text-success">Shreethemes</a>.</p>
            </div>
        </footer>
		</>
	)
}