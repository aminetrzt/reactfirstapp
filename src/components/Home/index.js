import React from 'react'


import Header from '../header'
import Main from '../main'
import About from '../about'
import Hobbies from '../hobbies'
import Service from '../service'
import Resume from '../resume'
import Skill from '../skill'
import Latest from '../latest'
import Contactme from '../contactme'
import Email from '../email'
import Footer from '../footer'

export default function Home() {
    return ( <>
    		<Header/>
    		<Main/>
    		<About/>
    		<Hobbies/>
    		<Service/>
    		<Resume/>
    		<Skill/>
            <Latest/>
            <Contactme/>
    		<Email/>
    		<Footer/>
            </>
    )
}
