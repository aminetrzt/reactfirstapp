import React from 'react'

export default function About () {
	return (
		<>
			<div class="row align-items-center">
                    <div class="col-lg-5 col-md-5">
                        <div class="about-hero">
                            <img src="images/hero.png" class="img-fluid mx-auto d-block about-tween position-relative" alt="" style={{ transform : "matrix(1, 0, 0, 1, -7.09286, 3.5)" }}/>
                        </div>
                    </div>

                    <div class="col-lg-7 col-md-7 mt-4 pt-2 mt-sm-0 pt-sm-0">
                        <div class="section-title mb-0 ml-lg-5 ml-md-3">
                            <h4 class="title text-primary mb-3">Cristino Murphy</h4>
                            <h6 class="designation mb-3">I'm a Passionate <span class="text-primary">Web Designer</span></h6>
                            <p class="text-muted">Obviously I'm a Web Designer. Web Developer with over 3 years of experience. Experienced with all stages of the development cycle for dynamic web projects. The as opposed to using 'Content here, content here', making it look like readable English.</p>
                            <p class="text-muted">The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                            <img src="images/sign.png" alt="" height="22"/>
                            <div class="mt-4">
                                <a href="index.html#projects" class="btn btn-primary mouse-down">View Portfolio</a>
                            </div>
                        </div>
                    </div>
                </div>
		</>
	)
};