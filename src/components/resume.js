import React from 'react'


export default function () {
	return (
		<>
			<section class="section" id="resume">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <div class="titles">
                                <h4 class="title title-line text-uppercase mb-4 pb-4">Work Participation</h4>
                                <span></span>
                            </div>
                            <p class="text-muted mx-auto para-desc mb-0">Obviously I'm a Web Designer. Experienced with all stages of the development cycle for dynamic web projects.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="main-icon rounded-pill text-center mt-4 pt-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star fea icon-md-sm"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                        </div>
                        <div class="timeline-page pt-2 position-relative">
                            <div class="timeline-item mt-4">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="duration date-label-left border rounded p-2 pl-4 pr-4 position-relative shadow text-left">2015 - 2018</div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="event event-description-right rounded p-4 border float-left text-left">
                                            <h5 class="title mb-0 text-capitalize">UX Designer</h5>
                                            <small class="company">Vivo - Senior Designer</small>
                                            <p class="timeline-subtitle mt-3 mb-0 text-muted">The generated injected humour, or non-characteristic words etc. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis,</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
            
                            <div class="timeline-item mt-4">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 order-sm-1 order-2">
                                        <div class="event event-description-left rounded p-4 border float-left text-right">
                                            <h5 class="title mb-0 text-capitalize">Web Developer</h5>
                                            <small class="company">Oppo - HR Manager</small>
                                            <p class="timeline-subtitle mt-3 mb-0 text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacinia magna vel molestie faucibus. Donec auctor et urnaLorem ipsum dolor sit amet.</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 order-sm-2 order-1">
                                        <div class="duration duration-right rounded border p-2 pl-4 pr-4 position-relative shadow text-left">2012 - 2015</div>
                                    </div>
                                </div>
                            </div>
        
                            <div class="timeline-item mt-4">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="duration date-label-left border rounded p-2 pl-4 pr-4 position-relative shadow text-left"> 2012 - 2010</div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="event event-description-right rounded p-4 border float-left text-left">
                                            <h5 class="title mb-0 text-capitalize">Graphic Designer</h5>
                                            <small class="company">Apple - Testor</small>
                                            <p class="timeline-subtitle mt-3 mb-0 text-muted">Therefore always free from repetition, injected humour, or non-characteristic words etc. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		</>
	)
}